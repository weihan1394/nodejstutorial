#Welcome to NodeJS Tutorial
---------------------------------------
This repository provides simple modules that are commonly used in NodeJS. Feel free to clone it and try it out.


##Installation
---------------------------------------
###IDE
Visual Studio Code Community
https://code.visualstudio.com/

###Useful IDE extension
* [Node JSON Autocomplete](https://marketplace.visualstudio.com/items?itemName=bhshawon.node-json-autocomplete
) - Auto complete extension

###Repository
* [Git](https://git-scm.com/download/win)
* [TortoiseGit](https://tortoisegit.org/)

###NodeJS
* [NodeJS](https://nodejs.org/en/download/)

##Running the file
---------------------------------------
1. View -> Terminal    [Ctrl + `]
2. [Ctrl + Shift + P] -> ENTER [default] -> SELECT [Terminal: Select Default Shell] -> SELECT [Command Prompt]
3. At the terminal run the file by [node tut01] //default it will be running a js file so you can exclude entering tut01.js 