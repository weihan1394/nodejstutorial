// HTTP Server with static files

const http = require('http');
const fs = require('fs');
http.createServer((req, res)=>{
    // Send html file
    // const readStream = fs.createReadStream('./example10/index.html');
    // // set header to let user know what data they are expecting
    // res.writeHead(200, {'Content-type': 'text/html'});

    // Send json file
    // const readStream = fs.createReadStream('./example10/example.json');
    // res.writeHead(200, {'Content-type': 'application/json'});

    // Send image file
    const readStream = fs.createReadStream('./example10/example.png');
    res.writeHead(200, {'Content-type': 'image/png'});

    // pipe readstream to send to the user
    readStream.pipe(res);
}).listen(3000);