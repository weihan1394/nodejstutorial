// Express Get requests
const express = require('express');
const app = express();

// route /
app.get('/', (req, res)=>{
    res.send('Hello World!');
});

// route /example
app.get('/example', (req, res)=>{
    res.send('hitting example route');
});

app.get('/example/:name/:age', (req, res)=>{
    console.log(req.params);

    // Take note: Only can send 1 result back to the client
    // res.send('example with params');
    // http://localhost:3000/example/weihan/24
    res.send(req.params.name + " : " + req.params.age);

    // Display query 
    // http://localhost:3000/example/weihan/24?friend=songwei
    // http://localhost:3000/example/weihan/24?friend=songwei&friend2=giokkhim
    // route param => compulsory have the field
    // query param => optional
    console.log(req.query);

    // Static file with express
});

app.listen(3000);