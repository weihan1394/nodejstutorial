// Basic File System Module
const fs = require('fs');

// create a  file
fs.writeFile('tut04example.txt', "this is an example", (err)=>{
    if (err)
        console.log(err);
    else {
        console.log("File successsfully created!");
        // must specify the encode type of the file else it will show the binary of the content
        // e.g. <Buffer 74 68 69 73 20 69 73 20 61 6e 20 65 78 61 6d 70 6c 65>
        fs.readFile('tut04example.txt', 'utf8', (err, file) => {
            if (err)
                console.log(err);
            else
                console.log(file);
        });
    }
});

// rename file
fs.rename('tut04example.txt', 'tut04example02.txt', (err) =>{
    if (err)
        console.log(err);
    else
        console.log('Successfully renamed the file!');
});

// append data to the file
fs.appendFile('tut04example02.txt', 'Some data being appended', err=>{
    if (err)
        console.log(err);
    else
        console.log('Successfully appended the file!');
});

// delete the file
fs.unlink('tut04example02.txt', (err)=>{
    if (err)
        console.log(err);
    else
        console.log('Successfully deleted the file!');
});