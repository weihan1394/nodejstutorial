// Tutorial 2 (EventEmitter)
// get the reference EventEmitter class of events module
const EventEmitter = require('events');
// create object of EventEmitter class by using above reference
const eventEmitter = new EventEmitter();
// subscribe for FirstEvent
// event will only fire when event tutorial occured
eventEmitter.on('tutorial', (num1, num2)=>{
    console.log('tutorial event has occured');
    var sum = num1 + num2;
    // check if it is NaN return true or false 
    console.log('sum = ' + isNaN(sum) ? sum: 0);
    // check if it's a null
    // console.log(isNaN(sum) ? 0: sum);
});

eventEmitter.emit('tutorial', 1, 2);


class Person extends EventEmitter{
    constructor(name){
        super();
        this._name = name;
    }

    get name(){
        return this._name;
    }
}

let james = new Person('James');
james.on('name', ()=>{
    console.log('my name is ' + james.name);
})

let christian = new Person('Christian');
christian.on('name', ()=>{
    console.log('my name is ' + james.name);
})


james.emit('name');
christian.emit('name');