// User input validation with Express and JOI at server side (part 2)
// npm i joi
const Joi = require('joi');
const arrayString = ['apple', 'banana', 'cake'];
const arrayObjects = [{example: 'example1'}, {example: 'example2'}];

const userInput = { personalInfo: {
    streetAddress: 'Blk 999 Serangoon North Ave 4',
    city: 'Singapore',
    state: 'SG'
}, preferences: arrayObjects};

const personalInfoSchema = Joi.object().keys({
    streetAddress: Joi.string().trim(),
    city: Joi.string().trim().required(),
    state: Joi.string().trim().length(2).required()

});

// Joi.array().items(Joi.string());
const preferencesSchema = Joi.array().items(Joi.object().keys({
    example: Joi.string().trim().required()
}));

const schema = Joi.object().keys({
    personalInfo: personalInfoSchema,
    preferences: preferencesSchema
})

Joi.validate(userInput, schema, (err, res)=>{
    if (err)
        console.log(err);
    else
        console.log(res);
})