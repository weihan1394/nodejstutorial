// Create package.json using npm init
// package json file holds all the meta data of the project (e.g. name of the project, version)

/*
1) npm install
2) Description is as below:

package name: (tutorial)
version: (1.0.0)
description: tutorial 11
entry point: (app.js) tut11.js
test command:
git repository:
keywords:
author: weihan
license: (ISC)
About to write to E:\Tutorial\NodeJS\Tutorial\package.json:

{
  "name": "tutorial",
  "version": "1.0.0",
  "description": "tutorial 11",
  "main": "tut11.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "weihan",
  "license": "ISC"
}


Is this OK? (yes)
*/

const lodash = require('lodash');
let example = lodash.fill([1, 2, 3, 4, 5], "banana", 1, 4);
console.log(example);


// uninstall package
//npm uninstall lodash