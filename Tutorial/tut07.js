const fs = require('fs');

// read file method 1
// read the whole file buffer which requires the file to load into the memory
fs.readFile('./example07/largeFile.txt', (err, file)=>{
    if (err)
        console.log(err);
    else
        console.log(file);
});

// read file method 2 (chunk)
const readStream = fs.createReadStream('./example07/largeFile.txt', 'utf8');
readStream.on('data', (chunk)=>{
    console.log(chunk);
});