// Readable and Writable streams
// reading and writing more efficently where it is all done in chunks and not all in once

const fs = require('fs');
const readStream = fs.createReadStream('./example06/lorez01.txt', 'utf8');
const writeStream = fs.createWriteStream('./example06/lorez02.txt');

readStream.on('data', (chunk)=>{
    // write to a new file
    writeStream.write(chunk);

    // display in console
    console.log(chunk);
});