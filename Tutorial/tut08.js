// Pipes and pipe chaining
const fs = require('fs');
const zlib = require('zlib'); // compress data
const gzip = zlib.createGzip();
const gunzip = zlib.createGunzip();
// const readStream = fs.createReadStream('./example08/file1.txt', 'utf8');
// const writeStream = fs.createWriteStream('./example08/file2.txt');
// const writeStreamGZ = fs.createWriteStream('./example08/file2.txt.gz');

const readStreamCompressed = fs.createReadStream('./example08/file2.txt.gz');
const writeStreamUncompressed = fs.createWriteStream('./example08/uncompressed.txt');

// Method 1: read stream and write to a new file
// readStream.on('data', chunk=>{
//     writeStream.write(chunk);
// });

// Method 2: pipe data from source to destination
// source: readStream       destination: writeStream
// readStream.pipe(writeStream);

// gzip source 
// readStream.pipe(gzip).pipe(writeStreamGZ);

// gunzip compressed file
try {
    readStreamCompressed.pipe(gunzip).pipe(writeStreamUncompressed);
} catch (error) {
    console.log(error);
}


// common error
/*
Error: unexpected end of file
=> You cannot write gzip and gunzip concurrently as it is all done in a chunk
=> Possible chance to hit the part where it is still trying to zip a file and unzip a file concurrently
*/