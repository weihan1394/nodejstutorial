// HTTP Post request w express and body parse module
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

// Using middlewear 
// / => refers to the alias (do not want others to know the folder is call static)
app.use('/public', express.static(path.join(__dirname, 'example15')));
app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json());

// Default path /
app.get('/', (req, res)=>{
    res.sendFile(path.join(__dirname, 'example15', 'index.html'));
}).listen(3000);

// Handle POST call
app.post('/', (req, res)=>{
    console.log(req.body);
    res.json({success : true})
});

// Go to Google Chrome => Enter [F12] => Inspect source 
// Make sure it returns you successfully got response