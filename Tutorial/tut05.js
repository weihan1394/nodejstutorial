// Advanced File System Module (Folders)

const fs = require('fs');

// create directory
/*
fs.mkdir('tutorialDirectory', (err)=>{
    if (err)
        console.log(err);
    else {
        
        // success
        console.log('Folder successfully created!');

        // remove directory
        fs.rmdir('tutorialDirectory', err=>{
            if (err)
                console.log(err);
            else
                console.log('Successfully deleted the folder!');
        })
        
        // create a file in a directory
        // (./) => refers to the relative directory of node js
        fs.writeFile('./tutorialDirectory/example.txt', '123', (err)=>{
            if (err)
                console.log(err);
            else
                console.log('Successfully created file!');
        });
    }
});

// remove file first else it will prompt error
fs.unlink('./tutorialDirectory/example.txt', (err)=>{
    if (err)
        console.log(err);
    else
        console.log("Successfully deleted file!");
})

// remove directory
fs.rmdir('tutorialDirectory', (err)=>{
    if (err)
        console.log(err);
    else
        console.log('Successfully deleted folder!')
})
*/

// remove all files from a directory
// create a folder with 2 files {a.txt b.txt}
fs.readdir('example', (err, files)=>{
    if (err)
        console.log(err);
    else {
        // console.log(files);
        // for loop to delete the files
        for (let file of files) {
            fs.unlink('./example/' + file, (err)=>{
                if (err)
                    console.log(err);
                else
                    console.log('Successfully deleted file!')
            })
        }

        fs.rmdir('example', (err)=>{
            if (err)
                console.log(err);
            else
                console.log('Successfully deleted folder!')
        })
    }
})