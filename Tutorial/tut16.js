// User input validation with Express and JOI
// npm i joi
const express = require('express');
const path = require('path');

const bodyParser = require('body-parser');
const app = express();

const Joi = require('joi');

// Using middlewear 
// / => refers to the alias (do not want others to know the folder is call static)
app.use('/public', express.static(path.join(__dirname, 'example16')));
app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json());

// Default path /
app.get('/', (req, res)=>{
    res.sendFile(path.join(__dirname, 'example16', 'index.html'));
}).listen(3000);

// Handle POST call
app.post('/', (req, res)=>{
    console.log(req.body);
    // validate data from the body
    // create schema
    const schema = Joi.object().keys({
        // set the rules of the data we receive
        username : Joi.string().trim().required(),
        password : Joi.string().min(5).max(10).required()
    });

    // Example of schema: 
    // { username: 'weihan1394@gmail.com', password: 'password' }

    /*
    In tutorial 15 the return is a serialized object 
    [   { name: 'firstname', value: 'Mickey' },
        { name: 'lastname', value: 'Mouse' }    ]
    */

    Joi.validate(req.body, schema, (err, result)=>{
        if (err) {
            // something went wrong
            console.log('error : ' + err);
            res.send('an error has occured');
        }
        else
        {
            console.log('successfully login!');
            res.send('successfully posted data');
        }
    });
});
