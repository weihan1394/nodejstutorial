// Tutorial 1 (Calling functions)
console.log('hello world from nodejs');

// at the same exact path
// dont need to specify the file name it will assume it is a node js file
const tutorial = require('./tutorial');
console.log(tutorial);
// sum(1, 1);
// console.log(tutorial(1,1));
console.log(tutorial.sum(1,1));
console.log(tutorial.PI);
console.log(new tutorial.SomeMathObject());