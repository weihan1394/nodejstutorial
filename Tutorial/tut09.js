// Creating a http server

const http = require('http');
const server = http.createServer((req, res)=>{
    if (req.url === '/') {
        res.write('Hello world from nodejs');
    }
    else {
        res.write('Using other domain');
    }
    res.end();
});

// server listen to port 3000
server.listen('3000');