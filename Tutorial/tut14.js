// Express with static files
const express = require('express');
const path = require('path');
const app = express();

// Using middlewear 
// / => refers to the alias (do not want others to know the folder is call static)
app.use('/public', express.static(path.join(__dirname, 'example14')));

app.get('/', (req, res)=>{
    res.sendFile(path.join(__dirname, 'example14', 'index.html'));
}).listen(3000);
